#!/bin/bash

echo "Number of history $1 months"
dateClean=$(date --date="$1 months ago" +%Y-%m-%d)
echo "Date Clean ${dateClean}"

database='PTTUsageStatistics'

function purge () {
  local database=$1
  local table=$2
  local dateClean=$3
  local requete='DELETE FROM '\"${table}\"' WHERE time < '\'${dateClean}\''; '

  echo "Purge database:${database} table:${table} Clean before:${dateClean}"
  echo influx -database ${database} -username \'${USER_CREDENTIALS_USR}\' -password \'${USER_CREDENTIALS_PSW}\' -host '172.23.150.80' -precision 'rfc3339' -execute "${requete}"
  influx -database ${database} -username \'${USER_CREDENTIALS_USR}\' -password \'${USER_CREDENTIALS_PSW}\' -host '172.23.150.80' -precision 'rfc3339' -execute "${requete}"
}

purge "${database}"  "actionsResultsInEachExecution" "${dateClean}"
purge "${database}"  "testRes" "${dateClean}"
purge "${database}"  "actionsResultsForReports" "${dateClean}"
purge "${database}"  "actionsResultsInEachExecutionByCampaign" "${dateClean}"
